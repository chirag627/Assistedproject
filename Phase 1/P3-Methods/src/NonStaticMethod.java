
public class NonStaticMethod {
	void addNum(int a, int b) {
		int result = a + b;
		System.out.println(result);

	}

	public static void main(String[] args) {
		NonStaticMethod m = new NonStaticMethod();
		m.addNum(20, 30);
	}
}