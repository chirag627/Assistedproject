class Parent {
	protected void display() {
		System.out.println("Using protected access modifier, no class can use it without extending the class");
	}
}

class ChildOne extends Parent {
}

class ChildTwo extends Parent {
}

public class ProtectedAccessModifier {
	public static void main(String args[]) {
		ChildOne c1obj = new ChildOne(); // create object of class ChildOne
		c1obj.display(); // access class A protected method using obj
		ChildTwo c2Obj = new ChildTwo(); // create object of class ChildTwo
		c2Obj.display(); // access class A protected method using cObj
	}
}