class BaseClassA {
	public void display() {
		System.out.println("Using public access Modifier");
	}
}

public class PublicAccessModifier {
	public static void main(String args[]) {
		BaseClassA obj = new BaseClassA();
		obj.display();
	}
}